﻿using System.Windows;
using System.Windows.Controls;

namespace Calculatrice
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private OperationViewModel viewModel = new OperationViewModel();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void NumberButton_Click(object sender, RoutedEventArgs e)
        {
            if (!(sender is Button button))
                return;
            viewModel.AddNumber(button.Content as string);
        }

        private void OperationButton_Click(object sender, RoutedEventArgs e)
        {
            if (!(sender is Button button))
                return;
            viewModel.AddOperator(button.Content as string);
        }

        private void ResultButton_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.ComputeResult())
                viewModel.Clear();
        }
    }
}