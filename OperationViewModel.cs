﻿using System.Reflection.Emit;

namespace Calculatrice
{
    public class OperationViewModel : BaseViewModel
    {
        private string operand1;
        private string operand2;
        private string operateur;

        private string currentNumber = "0";
        private string currentOperation;
        
        private bool hasOperand1 => !string.IsNullOrEmpty(operand1);
        private bool hasOperand2 => !string.IsNullOrEmpty(operand2);
        private bool hasOperateur => !string.IsNullOrEmpty(operateur);

        public string CurrentNumber
        {
            get { return currentNumber; }
            set
            {
                currentNumber = value;
                OnPropertyChanged();
            }
        }

        public string CurrentOperation
        {
            get { return currentOperation; }
            set
            {
                currentOperation = value;
                OnPropertyChanged();
            }
        }

        public void Clear()
        {
            operand1 = "";
            operand2 = "";
            operateur = "";
        }

        public void AddNumber(string value)
        {
            // Reset de l'affichage de l'opération si nouvelle opération
            if (!hasOperand1)
                CurrentOperation = "";

            // Reset de l'affichage du nombre si nouveau nombre
            if (!hasOperand1 || (hasOperateur && !hasOperand2))
                CurrentNumber = "";

            if (!hasOperateur)
                AddToOperand(ref operand1, value);
            else
                AddToOperand(ref operand2, value);

            CurrentNumber += value;
        }

        public void AddOperator(string value)
        {
            if (!hasOperand1 || hasOperand2 || hasOperateur)

                return;
            CurrentOperation = $"{operand1} {value} ";
            operateur = value;
        }

        public bool ComputeResult()
        {
            if (!hasOperand1 || !hasOperand2 || !hasOperateur)
                return false;

            double result = 0;
            double number1 = double.Parse(operand1);
            double number2 = double.Parse(operand2);

            switch (operateur)
            {
                case "+":
                    result = number1 + number2;
                    break;

                case "-":
                    result = number1 - number2;
                    break;

                case "x":
                    result = number1 * number2;
                    break;

                case "/":
                    result = number1 / number2;
                    break;

                default:
                    result = double.NaN;
                    break;
            }

            CurrentNumber = $"{result:0.######}";
            CurrentOperation += $"{operand2} =";
            return true;
        }

        private void AddToOperand(ref string operand, string value)
        {
            if (!double.TryParse(operand + value, out _))
                return;

            operand += value;
        }
    }
}